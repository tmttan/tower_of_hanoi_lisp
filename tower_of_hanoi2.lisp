(defun tower-of-hanoi()
  (setf number-of-tiles 3)
  (setf array-of-poles (make-array '(3)))
  (setf number-of-moves 0)
  (setf valid-moves -1)
  (setf current-move 0)
  (setf exit-game 0)
  (setf array-of-choices (make-array '(5)))
  (setf evaluation-result nil)
  
  
  ;initialize poles 
  (setf array-of-poles (initialize-poles array-of-poles number-of-tiles))
  ;print intro to game
  (print-intro)
  
  
  (do
	  ()
	  ((eq exit-game 1) (exit-game-info))
	
      ;print the poles
	  (print-poles array-of-poles)
	  ;print moves-made
	  (moves-made number-of-tiles number-of-moves)
	  ;print possible options
	  ;(generate-possible-options array-of-poles)
	  (setf valid-moves -1)
	  (cond
	    (
		  (null evaluation-result)
		  (setf array-of-choices (generate-possible-options array-of-poles))
	      (setf valid-moves (print-possible-options array-of-choices))
		)
	  )
	  ;print default options
	  (print-default-options)
	  ;get-option 
	  (setf current-move (get-option valid-moves))
	  (cond
		(
		  (eq current-move 0) ; exit game
		  (setf exit-game 1)
		)
		(
		 (eq current-move -1) ; restart game
		 (setf number-of-tiles (get-n-moves))
		 (setf number-of-moves 0)
		 (setf evaluation-result nil)
		 (setf array-of-poles (initialize-poles array-of-poles number-of-tiles))
		)
		(
		  t 
		  (setf array-of-poles (execute-option (aref array-of-choices (- current-move 1) ) array-of-poles))
		  (setf evaluation-result (evaluate-option array-of-poles number-of-tiles)) 
		  (cond
			(
			  (eq evaluation-result t)
			  (setf number-of-moves (+ number-of-moves 1))
			  (format t "Congratulations! You finished the game in ~a moves! ~%" number-of-moves)
			)
			(
			  (eq evaluation-result nil)
			  ;(print "ola")
			  (setf number-of-moves (+ number-of-moves 1))
			
			)
		  )
		)
	  ) 
  
  );;;;end of do loop



);;;; end of tower-of-hanoi function

(defun exit-game-info ()
 (format t "Goodbye! See you again another time :D ~%")
 (values)
)
(defun get-n-moves()
  (setf num -1)
  
  (loop 
   (format t "Enter new N value: ~%")
   (setf num (read))
   (when (and (numberp num) (> num 0)) (return num))
  )
)
(defun transform-move (node)
  (cond
    ( 
		(string-equal node "a")
		(return-from transform-move 0)
	)
	( 
		(string-equal node "b")
		(return-from transform-move 1)
	)
	( 
		(string-equal node "c")
		(return-from transform-move 2)
	)
  )
)
(defun evaluate-option (array-of-poles number-of-tiles)
 (setf last-pole (aref array-of-poles 2)) ; get last pole 
 (cond
   (
     (and (not (null last-pole)) (eq (length last-pole) number-of-tiles))
	 (return-from evaluate-option t)
   )
   (
     t
	 (return-from evaluate-option nil)
   )
 )

)

(defun check-option-is-valid (origin destination)
	(if (or (eq origin nil) (eq destination nil)) (return-from check-option-is-valid T) (if (< origin destination) (return-from check-option-is-valid T) (return-from check-option-is-valid nil))
))

(defun check-option (option valid-moves)

 ; (format t "option: ~a , is sitring : ~a , is number : ~a ~%" option (stringp option) (numberp option) )
 ; (format t "string equal ~a ~%" (string-equal option "x"))
 ; (format t "valid move ~a ~%" valid-moves)
  (cond
    (
	  (not (numberp option))
	  (cond
	   (
	     (or (string-equal option "x") (string-equal option "r"))
		 (return-from check-option t)
	   )
	   ( t 
		  (format t "You entered an invalid option. Try again.~%") 
		  (return-from check-option NIL)
		   
		)
	  )
	)
	(
	  (numberp option)
	  (cond
	   (
	     (and (> option 0) (<= option valid-moves))
		 (return-from check-option t)
	   )
	   ( t 
		  (format t "You entered an invalid option. Try again.~%") 
		  (return-from check-option NIL)
		   
		)
	  )
	)
	( t 
	  (format t "You entered an invalid option. Try again.~%") 
	  (return-from check-option NIL)
	   
	)
  
  )
)

(defun execute-option (move array-of-poles)
  ;(format t "~a ~%" move)
  (setf source-pole-index (transform-move(first move)))
  (setf dest-pole-index (transform-move(first (last move))))
  (setf source-pole (aref array-of-poles source-pole-index))
  (setf dest-pole (aref array-of-poles dest-pole-index))

  (setf source-pole-head (last source-pole)) ; get head of source 
  (setf dest-pole-head (last dest-pole)) ; get head of destination 
  ;(format t "~a ~%" source-pole-index)
  ;(format t "~a ~%" dest-pole-index)

  	(cond
	   	(
			(check-option-is-valid (first source-pole-head) (first dest-pole-head))
			(progn
				(setf source-pole (butlast source-pole)); remove head of source
				(setf dest-pole (append dest-pole source-pole-head)) ; append head to dest
				
				;(format t "~a ~%" source-pole)
				;(format t "~a ~%" dest-pole)
				
				(setf (aref array-of-poles source-pole-index) source-pole) ;set the array of pole index
				(setf (aref array-of-poles dest-pole-index) dest-pole)
				
				(return-from execute-option array-of-poles)
			)
	   	)
	   	( t 
			(format t "You entered an invalid option. Try again.~%") 
			(return-from execute-option array-of-poles)
		)
	)
)
(defun transform-option (option)
  (cond
    (
	  (not (numberp option))
	  (cond
	   (
	     (string-equal option "x")
		 (return-from transform-option 0)
	   )
	   (
	     (string-equal option "r")
		 (return-from transform-option -1)
	   )
	  )
	)
	(
	  (numberp option)
	  (return-from transform-option option)
	)

  
  )

)
(defun get-option (valid-moves)
  (setf option -1)
  
  (loop 
   (format t "Your choice? ~%")
   (setf option (read))
   (when (check-option option valid-moves) (return (transform-option option)))
  )

)
(defun moves-made (number-of-tiles number-of-moves) 
  (format t "~%~%Minimum moves needed: ~a ~%" (-(expt 2 number-of-tiles) 1))
  (format t "Number of moves made: ~a ~%" number-of-moves)

)
(defun print-possible-options (array-of-choices)
   (setf array-length (length array-of-choices))
   (setf valid-choices 0)
   (format t "~%~%Next move: ~%")
   (dotimes (n array-length)
      (cond
        (
			(not (eq (aref array-of-choices n) nil))
			(setf valid-choices (+ valid-choices 1))
			(format t "~a : ~a ~%" (+ n 1) (aref array-of-choices n))
		)
	  
      
	  )
   )
   (return-from print-possible-options valid-choices)

)
(defun print-default-options ()
   (format t "R : Restart the game ~%")
   (format t "X : Exit the game ~%")
   (values)
)
(defun get-char (num)
  (cond
    ( (eq num 0) (return-from get-char `A))
    ( (eq num 1) (return-from get-char `B))
	( (eq num 2) (return-from get-char `C))
  )

)
(defun generate-possible-options (array-of-poles) ; get valid options only
   (setf array-of-choices (make-array '(5))) ; max of 5 options 
   (setf choice-index 0)
   (dotimes (n 3)
      (setf current-pole (aref array-of-poles n))
	  (cond
	    (
		  (not (null (first current-pole)))
		  (setf firstNode (first current-pole))
          (dotimes (m 3)
		     (cond 
			  (
			    (not (= n m))
				(setf firstNodeOther (first (aref array-of-poles m)))
				;(format t "~a ~a ~%" firstNode firstNodeOther)
				(cond 
				  (
				  (or (eq firstNodeOther nil) (< firstNode firstNodeOther) )
				  (setf (aref array-of-choices choice-index) (list (get-char n) (get-char m)))
				  (setf choice-index (+ choice-index 1))
				  )
				)
			  )
			 
			 )
		  )		  
		  
		)
		
	  
	  )     
   )
  ; (setf (aref array-of-choices choice-index) "(R) - restart the game")
   ;(setf choice-index (+ choice-index 1))
   ;(setf (aref array-of-choices choice-index) "(X) - exit the game")
   (return-from generate-possible-options array-of-choices)   

)
(defun initialize-poles(array-of-poles number-of-tiles)
    (setf new-pile ())
	(setf i 0)
	(dotimes (n number-of-tiles)
	   (setf new-pile (append new-pile (list (- number-of-tiles i) )))
	   (setf i (+ i 1))	
	)
	(setf (aref array-of-poles 0) new-pile)
	(setf (aref array-of-poles 1) nil)
	(setf (aref array-of-poles 2) nil)
	(return-from initialize-poles array-of-poles) 
)
(defun print-poles(array-of-poles)
    (format t "Current state: ~%")
   	(dotimes (n 3)
	   (cond
	      (
		    (null (aref array-of-poles n))
			(format t "~a ~%" nil)
		  
		  ) 
		  (
		    (not (null (aref array-of-poles n)))
              		 (dolist (item (aref array-of-poles n)) 
		            (format t "~a " item)
			 )
			 (format t "~%")
		  )
	   
	   )	
	)
    (values)

)
(defun print-intro ()
   (format t "*********************~%")  
   (format t "*The Tower of Hanoi *~%")
   (format t "* Interactive Game  *~%")
   (format t "*   version 1.0     *~%")
   (format t "*********************~%")
   (values)
)