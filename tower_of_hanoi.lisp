(defun Hanoi (n origin destination auxiliar)
(if (= n 1) (moure 1 origin destination auxiliar)
    (progn (Hanoi (- n 1) origin auxiliar destination)
        (moure n origin destination auxiliar)
        (Hanoi (- n 1) auxiliar destination origin))))
        
(defun moure (k origin destination auxiliar)
    (print (list origin destination auxiliar)))

(Hanoi 3 '1 '3 '2)