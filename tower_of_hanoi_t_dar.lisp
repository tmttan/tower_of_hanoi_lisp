(defun tower-of-hanoi()
  (setf number-of-tiles 3)
  (setf array-of-poles (make-array number-of-tiles))

  ;initialize poles 
  (setf array-of-poles (initialize-poles array-of-poles number-of-tiles))
  ;print intro to game
  (print-intro)
  ;print the poles
  (print-poles array-of-poles)
  ;print possible options
  ;(generate-possible-options array-of-poles)
  (setf possible-options (generate-possible-options array-of-poles))

  (print-possible-options (generate-possible-options array-of-poles))

; if optionInput X return nil
; if optionInput Get N of poles then pass to generate-possible-options
; else evaluateOption
(defun contains (item sequence) (if (member item sequence) T NIL))

(defun check-option (option, possible-moves)  
    (cond ((eq option -1) (write "Exit Game"))    
        ((eq option 0) (write "Restart Game")) 
        ((contains option possible-moves) (write "Move Poles")) 
        (t           (write "Invalid Option"))
    )
)

; return the remaining item in the list after getting the first item
; x = 2 ; lst = 1 2 3 => result = (3)
; x = 1 ; lst = 1 2 3 => result = (2 3)
(defun execute-option (origin destination)
  ; (cond ((eq lst nil) nil)
  ;     ((eq x (car lst)) (cdr lst))
  ;     (t (execute-option x (cdr lst)))
  ; )
  (setf number-of-tiles 3)
  (setf array-of-poles (make-array number-of-tiles))
  (setf array-of-poles (initialize-poles array-of-poles number-of-tiles))

  (setf first-pole (aref array-of-poles 0))
  (setf second-pole (aref array-of-poles 1))
  (setf last-pole (aref array-of-poles 2))
  

  ; (setf second-pole (append (first first-pole)))
  ; (setf first-pole (cdr first-pole))

  (cond ((eq origin 1) (
      cond 
      (
        (eq destination 2) 
        (setf nodetoMove (first first-pole))
        (setf second-pole (append nodetoMove))
        (setf first-pole (cdr first-pole))
      )
      ((eq destination 3) (write "1 to 3"))
      (t (write "Invalid Move"))
    ))
    ((eq origin 2) (
      cond ((eq destination 1) (write "2 to 1"))
      ((eq destination 3) (write "2 to 3"))
      (t (write "Invalid Move"))
    ))
    ((eq origin 3) (
      cond ((eq destination 1) (write "3 to 1"))
      ((eq destination 2) (write "3 to 2"))
      (t (write "Invalid Move"))
    ))
  )

  (write first-pole)
  (write second-pole)
  (write last-pole)
)

(defun print-possible-options (array-of-choices)
   (setf array-length (length array-of-choices))
   (format t "~%~%Next move: ~%")
   (dotimes (n array-length)
      (if (not (eq (aref array-of-choices n) nil))
      (format t "~a : ~a ~%" (+ n 1) (aref array-of-choices n))
	  )
   )
   (format t "0 : Restart the game ~%")
   (format t "-1 : Exit the game ~%")
   (values)
)

(defun get-char (num)
  (cond
    ( (eq num 0) (return-from get-char `A))
    ( (eq num 1) (return-from get-char `B))
	( (eq num 2) (return-from get-char `C))
  )

)

(defun generate-possible-options (array-of-poles) ; get valid options only
   (setf array-of-choices (make-array '(5))) ; max of 5 options 
   (setf choice-index 0)
   (dotimes (n 3)
      (setf current-pole (aref array-of-poles n))
	  (cond
	    (
		  (not (null (first current-pole)))
		  (setf firstNode (first current-pole))
          (dotimes (m 3)
		     (cond 
			  (
			    (not (= n m))
				(setf firstNodeOther (first (aref array-of-poles m)))
				;(format t "~a ~a ~%" firstNode firstNodeOther)
				(cond 
				  (
				  (or (eq firstNodeOther nil) (> firstNode firstNodeOther) )
				  (setf (aref array-of-choices choice-index) (list (get-char n) (get-char m)))
				  (setf choice-index (+ choice-index 1))
				  )
				)
			  )
			 
			 )
		  )		  
		  
		)
		
	  
	  )     
   )
  ; (setf (aref array-of-choices choice-index) "(R) - restart the game")
   ;(setf choice-index (+ choice-index 1))
   ;(setf (aref array-of-choices choice-index) "(X) - exit the game")
   (return-from generate-possible-options array-of-choices)   
)

(defun initialize-poles(array-of-poles number-of-tiles)
    (setf new-pile ())
	(dotimes (n number-of-tiles)
	   (setf new-pile (append new-pile (list (+ n 1) )))
	)
	(setf (aref array-of-poles 0) new-pile)
	(return-from initialize-poles array-of-poles) 
)

(defun print-poles(array-of-poles)
    (format t "Current state: ~%")
   	(dotimes (n 3)
	   (cond
	      (
		    (null (aref array-of-poles n))
			(format t "~a ~%" nil)
		  
		  ) 
		  (
		    (not (null (aref array-of-poles n)))
              		 (dolist (item (aref array-of-poles n)) 
		            (format t "~a " item)
			 )
			 (format t "~%")
		  )
	   
	   )	
	)
    (values)

)
(defun print-intro ()
   (format t "*********************~%")  
   (format t "*The Tower of Hanoi *~%")
   (format t "* Interactive Game  *~%")
   (format t "*   version 1.0     *~%")
   (format t "*********************~%")
   (values)
)

(execute-option 1 2) 